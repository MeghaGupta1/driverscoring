# DriverScoring

**Pre-requisites**: Python Version 3 and above

1. Install the packages in requirement.txt
2. Run main.py. The server will start on port 5000.


**For publicly hosting the API:**
- Download and unzip ngrok https://ngrok.com/download
- Open ngrok.exe. Run 'ngrok http 5000'
