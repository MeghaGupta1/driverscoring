import pandas
# Columns names in CSV file.
TRIP_DATA_COLUMNS = [
    'time',
    'speed',
    'shift',
    'engine_Load',
    'car_accel',
    'rpm',
    'pitch',
    'lateral_acceleration',
    'passenger_count',
    'car_load',
    'ac_status',
    'window_opening',
    'radio_volume',
    'rain_intensity',
    'visibility',
    'driver_wellbeing',
    'driver_rush'
]
# Numerical features used for analysis.
NUMERICAL_FEATURES = [
    'speed',
    'car_accel',
    'lateral_acceleration',
    'rpm',
    'pitch',
    'shift'
]

# Categorical features used for analysis.
CATEGORICAL_FEATURES = [
    'driver_rush',
    'visibility',
    'rain_intensity',
    'driver_wellbeing'
]


def format_df(concatenated_df:pandas.DataFrame):
    max_time = 0
    concatenated_df.columns = TRIP_DATA_COLUMNS
    # Assign time to keep record continuity
    concatenated_df.time = concatenated_df.time + max_time
    max_time = concatenated_df.time.max()
    # Assign idx based on new index. Useful to brakedown events later on.
    concatenated_df['idx'] = concatenated_df.index
    return concatenated_df


def filter_acceleration_entries(df: pandas.DataFrame, threshold=2, above=True):
    """Extract acceleration events out of dataset.

    Parameters
    ----------
    df : pd.DataFrame
        Dataset containing driving measures.
    threshold : int
        Treshold for significant acceleration in m/s^2.
    above : boolean
        Should the acceleration be above or bellow threshold.

    Returns
    -------
    pd.DataFrame
        Dataframe containing only acceleration events driving measures.

    """
    return df[df.car_accel > threshold] if above else df[df.car_accel < threshold]

def extract_events(df: pandas.DataFrame, interval=2):
    """Extract a list of brake event time series.

    Parameters
    ----------
    df : pd.DataFrame
        Dataset containing driving measures.
    interval : int
        Time interval in seconds where 2 events are considered distincts.

    Returns
    -------
    list
        List of DataFrame containing events time series.

    """
    # prepare output
    events = []

    # Measures are taken at a 100Hz frequency.
    boundaries = (df.time - df.time.shift()) > (interval)
    new_boundaries = boundaries.reset_index()

    # boundaries_indexes = new_boundaries[new_boundaries['idx']].index
    boundaries_indexes = new_boundaries[new_boundaries['time']].index

    for i in range(len(boundaries_indexes)):
        min_bound = 0 if i == 0 else boundaries_indexes[i-1]
        max_bound = boundaries_indexes[i]

        if len(df[min_bound:max_bound]) > 10:
            events.append(df[min_bound:max_bound])

    return events


def calculate_acceleration_score(df):
    # Extracting acceleration events
    acceleration_entries_df = filter_acceleration_entries(df, threshold=2)
    acceleration_events = extract_events(acceleration_entries_df)
    events = len(acceleration_events)
    if events >= 30:
        score = 1
    elif 20 <= events < 30:
        score = 3
    elif 10 <= events < 20:
        score = 4
    elif 5 <= events < 10:
        score = 5
    elif 1 <= events < 5:
        score = 6
    else:
        score = 7
    return score


def calculate_braking_score(df):
    # Extracting braking events
    braking_entries_df = filter_acceleration_entries(df, threshold=-2, above=False)
    braking_events = extract_events(braking_entries_df)
    events = len(braking_events)
    if events >= 50:
        score = 0
    elif 35 <= events < 50:
        score = 1
    elif 20 <= events < 35:
        score = 2
    elif 10 <= events < 20:
        score = 3
    elif 5 < events < 10:
        score = 3.5
    else:
        score = 4
    return score


def calculate_pyhd_score(acc_score, brk_score):
    pyhd = round((acc_score/brk_score)*20)
    return pyhd


def calculate_individual_score(concatenated_df):
    df = format_df(concatenated_df)
    acc_score = calculate_acceleration_score(df)
    brk_score = calculate_braking_score(df)
    avg_score = (7 * acc_score + 4 * brk_score) / 2
    pyhd = calculate_pyhd_score(acc_score, brk_score)
    res = {
        'acc': acc_score,
        'brk': brk_score,
        'last_trip': avg_score,
        'pyhd': pyhd
    }
    return res