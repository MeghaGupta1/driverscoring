from data import calculate_individual_score
import boto3
import pandas
from flask_cors import CORS
from flask import Flask, render_template, jsonify, request
from flask_restful import Resource, Api

#Setting up the Flask app
app = Flask(__name__)
app.config["DEBUG"] = False
api = Api(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


client = boto3.client(
    's3',
    aws_access_key_id='AKIAUZE2WFS3LZEHP74D',
    aws_secret_access_key='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',
    region_name='us-east-1'
)

# Creating the high level object oriented interface
resource = boto3.resource(
    's3',
    aws_access_key_id='AKIAUZE2WFS3LZEHP74D',
    aws_secret_access_key='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',
    region_name='us-east-1'
)


@app.route('/calculateScore')
def fetch_user():
    driver = request.args.get('driver')
    res = calculate_score(driver)
    return jsonify(res)


@app.route('/overallScore')
def fetch_scores():
    res={
        'driver1': calculate_score('Driver1'),
        'driver2': calculate_score('Driver2'),
        'driver3': calculate_score('Driver3'),
    }
    for key, value in res.items():
        res[key]['name'] = key
    return jsonify(res)


def calculate_score(driver):
    response = client.list_objects_v2(
        Bucket='driver.details',
        Prefix=str(driver + '/'),
        MaxKeys=100)
    key = sorted(response['Contents'], key=lambda i: i['LastModified'])[-1]
    obj = client.get_object(
        Bucket='driver.details',
        Key=key['Key']
    )
    concatenated_df = pandas.read_csv(obj['Body'], header=None)
    res = calculate_individual_score(concatenated_df)
    return res


if __name__ == '__main__':
    print('PyCharm')
    app.run(host='0.0.0.0', port=5000)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
